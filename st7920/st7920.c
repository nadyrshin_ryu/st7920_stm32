//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include <delay.h>
#include <st7920.h>
#include <gpio.h>
#include <spim.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


#if (ST7920_RS_CS_Used)
  #define ST7920_RS_CS_HIGH()           GPIO_WriteBit(ST7920_RS_CS_Port, ST7920_RS_CS_Pin, Bit_SET) 
  #define ST7920_RS_CS_LOW()            GPIO_WriteBit(ST7920_RS_CS_Port, ST7920_RS_CS_Pin, Bit_RESET)
#else
  #define ST7920_RS_CS_HIGH()            
  #define ST7920_RS_CS_LOW()            
#endif

#if (ST7920_RESET_Used)
  #define ST7920_RESET_HIGH()             GPIO_WriteBit(ST7920_RESET_Port, ST7920_RESET_Pin, Bit_SET) 
  #define ST7920_RESET_LOW()              GPIO_WriteBit(ST7920_RESET_Port, ST7920_RESET_Pin, Bit_RESET)
#else
  #define ST7920_RESET_HIGH()            
  #define ST7920_RESET_LOW()            
#endif

#define ST7920_RW_HIGH()                GPIO_WriteBit(ST7920_RW_Port, ST7920_RW_Pin, Bit_SET) 
#define ST7920_RW_LOW()                 GPIO_WriteBit(ST7920_RW_Port, ST7920_RW_Pin, Bit_RESET)
#define ST7920_E_HIGH()                 GPIO_WriteBit(ST7920_E_Port, ST7920_E_Pin, Bit_SET) 
#define ST7920_E_LOW()                  GPIO_WriteBit(ST7920_E_Port, ST7920_E_Pin, Bit_RESET)
#define ST7920_SetDATA_4bit(val)        {GPIO_ResetBits(ST7920_Data_Port, 0xF << ST7920_Data_Shift); GPIO_SetBits(ST7920_Data_Port, val << ST7920_Data_Shift);}
#define ST7920_SetDATA_8bit(val)        {GPIO_ResetBits(ST7920_Data_Port, 0xFF); GPIO_SetBits(ST7920_Data_Port, val);}
#define ST7920_GetDATA_4bit()           ((GPIO_ReadInputData(ST7920_Data_Port) >> ST7920_Data_Shift) & 0xF)
#define ST7920_GetDATA_8bit()           (GPIO_ReadInputData(ST7920_Data_Port) && 0xFF)
#define ST7920_SetDATA_PinMode_In(mask)  gpio_SetGPIOmode_In(ST7920_Data_Port, mask, 0)
#define ST7920_SetDATA_PinMode_Out(mask) gpio_SetGPIOmode_Out(ST7920_Data_Port, mask)


uint8_t Init4bit;
uint8_t ST7920_Width, ST7920_Height;


//==============================================================================
// ��������� ������ ����� � �������
// - IsCmd - ������ ���������� ����� (����� ����� ������)
//==============================================================================
void ST7920_write(int8_t IsCmd, uint8_t Data)
{
#if (ST7920_IF == ST7920_IF_SPI)
  uint8_t StartByte = 0xF8;
  if (!IsCmd)
    StartByte |= ST7920_StartByte_RSmask;
  
  ST7920_RS_CS_HIGH();  // ChipSelect = 1
  
  // ���������� Start Byte
  SPI_send8b(ST7920_SPI_periph, &StartByte, 1);
  // ���������� High Byte
  StartByte = Data & 0xF0;
  SPI_send8b(ST7920_SPI_periph, &StartByte, 1);
  // ���������� Low Byte
  StartByte = Data << 4;
  SPI_send8b(ST7920_SPI_periph, &StartByte, 1);
  
  delay_us(ST7920_ShortDelayUs_5);
  
  ST7920_RS_CS_LOW();   // ChipSelect = 0
#else  
  if (IsCmd)
  {
    ST7920_RS_CS_LOW();
  }
  else
  {
    ST7920_RS_CS_HIGH();
  }

  ST7920_RW_LOW();
  
  #if (ST7920_IF == ST7920_IF_Parallel_4bit)
    // ����������� ���� ������ ��� ������
    ST7920_SetDATA_PinMode_Out(0xF << ST7920_Data_Shift);
  
    // ����� ������� �������
    ST7920_E_HIGH();
    ST7920_SetDATA_4bit(Data >> 4);
    delay_us(ST7920_ShortDelayUs_1);
    ST7920_E_LOW();

    delay_us(ST7920_ShortDelayUs_2);

    if (!Init4bit)
    {
      // ����� ������� �������
      ST7920_E_HIGH();
      ST7920_SetDATA_4bit(Data & 0xF);
      delay_us(ST7920_ShortDelayUs_1);
      ST7920_E_LOW();

      delay_us(ST7920_ShortDelayUs_2);
    }
    
    // ����������� ���� ������ ��� �����
    ST7920_SetDATA_PinMode_In(0xF << ST7920_Data_Shift);
  #else
    // ����������� ���� ������ ��� ������
    ST7920_SetDATA_PinMode_Out(0xFF);
  
    // ����� ����
    ST7920_E_HIGH();
    ST7920_SetDATA_8bit(Data);
    delay_us(ST7920_ShortDelayUs_3);
    ST7920_E_LOW();
  
    delay_us(ST7920_ShortDelayUs_4);

    // ����������� ���� ������ ��� �����
    ST7920_SetDATA_PinMode_In(0xFF);
  #endif
#endif
}
//==============================================================================


//==============================================================================
// Basic-������� ������� ������� � ��������� ������
//==============================================================================
void ST7920_Basic_Clear(void)
{
  ST7920_write(1, ST7920_CmdBasic_Clear);

  delay_us(1600);
}
//==============================================================================


//==============================================================================
// Basic-������� ��� ��������� ������� � ��������� ������ � ������
//==============================================================================
void ST7920_Basic_Home(void)
{
  ST7920_write(1, ST7920_CmdBasic_Home);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Basic-������� ��� ��������� ���������� ������ ������� � ������
//==============================================================================
void ST7920_Basic_EntryMode(uint8_t ShiftOn, uint8_t MoveRight)
{
  uint8_t Data = ST7920_CmdBasic_EntryMode;
  if (ShiftOn)
    Data |= (1 << 0);
  if (MoveRight)
    Data |= (1 << 1);
  
  ST7920_write(1, Data);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Basic-������� ��� ���������/���������� ������� � ���������� ������������
// ������� � ��������� ������
//==============================================================================
void ST7920_Basic_DisplayOnOff(uint8_t DisplayOn, uint8_t CursorOn, uint8_t BlinkOn)
{
  uint8_t Data = ST7920_CmdBasic_DisplayOnOff;

  if (DisplayOn)
    Data |= (1 << 2);
  if (CursorOn)
    Data |= (1 << 1);
  if (BlinkOn)
    Data |= (1 << 0);
  
  ST7920_write(1, Data);
  
  delay_us(72);
}
//==============================================================================


//==============================================================================
// Basic-������� ��� ��������� ���������� ������ �������
//==============================================================================
void ST7920_Basic_CursorDisplayControl(uint8_t DisplayMoveRight, uint8_t CursorMoveRight)
{
  uint8_t Data = ST7920_CmdBasic_CursorDisplayControl;

  if (DisplayMoveRight)
    Data |= (1 << 3);
  if (CursorMoveRight)
    Data |= (1 << 2);

  ST7920_write(1, Data);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Basic-������� ��� ���������� ������� ������� (Basic-Extended).
// ����� ��� ������������� �������� ������������� ����������.
//==============================================================================
void ST7920_Basic_FunctionSet(uint8_t ExtendedMode)
{
  uint8_t Data = ST7920_CmdBasic_FunctionSet;
  
#if (ST7920_IF == ST7920_IF_Parallel_8bit)
  Data |= (1 << 4);
#endif
  
  if (ExtendedMode)
    Data |= (1 << 2);
  
  ST7920_write(1, Data);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Basic-������� ��������� ��������� � CGRAM
//==============================================================================
void ST7920_Basic_SetCGRAMaddr(uint8_t Addr)
{
  uint8_t Data = ST7920_CmdBasic_SetCGRAMaddr;
  Data |= (Addr & 0x3F);
  ST7920_write(1, Data);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Basic-������� ��������� ��������� � DDRAM
//==============================================================================
void ST7920_Basic_SetDDRAMaddr(uint8_t Addr)
{
  uint8_t Data = ST7920_CmdBasic_SetDDRAMaddr;
  Data |= (Addr & 0x3F);
  ST7920_write(1, Data);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Extended-������� �������� � ����� ���
//==============================================================================
void ST7920_Ext_StandBy(void)
{
  ST7920_write(1, ST7920_CmdExt_StandBy);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Extended-������� �������� ��������� �� IRAM ��� Scroll-�����
//==============================================================================
void ST7920_Ext_SelScrollOrRamAddr(uint8_t SelectScroll)
{
  uint8_t Data = ST7920_CmdExt_SelScrollOrRamAddr;
  
  if (SelectScroll)
    Data |= 0x01;
      
  ST7920_write(1, Data);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Extended-������� �������� �������� 1 �� 4 �����. ��������� ����� ��������� ��������
//==============================================================================
void ST7920_Ext_Reverse(uint8_t Row)
{
  uint8_t Data = ST7920_CmdExt_Reverse;
  Data |= (Row & 0x03);
  ST7920_write(1, Data);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Extended-������� ��� ���������� ������� ������� (Basic-Extended), ����������
// ����������� �������. ����� ��� ������������� �������� ������������� ����������.
//==============================================================================
void ST7920_Ext_FunctionSet(uint8_t ExtendedMode, uint8_t GraphicMode)
{
  uint8_t Data = ST7920_CmdExt_FunctionSet;
  
#if (ST7920_IF == ST7920_IF_Parallel_8bit)
  Data |= (1 << 4);
#endif
  
  if (ExtendedMode)
    Data |= (1 << 2);
  
  if (GraphicMode)
    Data |= (1 << 1);
  
  ST7920_write(1, Data);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Extended-������� ��������� ��������� � IRAM ��� Scroll-������
//==============================================================================
void ST7920_Ext_SetIRAMOrSccrollAddr(uint8_t Addr)
{
  uint8_t Data = ST7920_CmdExt_SetIRAMOrSccrollAddr;
  Data |= (Addr & 0x3F);
  ST7920_write(1, Data);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// Extended-������� ��������� ��������� � ������ ����� ������������ ������
//==============================================================================
void ST7920_Ext_SetGDRAMAddr(uint8_t VertAddr, uint8_t HorizAddr)
{
  uint8_t Data = ST7920_CmdExt_SetGDRAMAddr;
  Data |= (VertAddr & 0x7F);
  ST7920_write(1, Data);
  
  Data = ST7920_CmdExt_SetGDRAMAddr;
  Data |= (HorizAddr & 0x0F);
  ST7920_write(1, Data);

  delay_us(72);
}
//==============================================================================


//==============================================================================
// ������� ���������� ���� - �������������� ������ �������� �� ��������� ������ 
// ����� ����������� ����������
//==============================================================================
uint8_t ST7920_GetHorizontalByte(uint8_t *pBuff, uint8_t Row, uint8_t Col)
{
  uint8_t Byte = 0;
  
  // ������� �������� (���������) ������ � ������� ��������� ������ �����
  uint16_t ByteIdx = (Row >> 3) * ST7920_Width;         // ������ � �������� ������ ������ ������
  ByteIdx += (Col << 3);
  
  // ���������� ������� �����, ���������� ������ �� 8 �������� � �������� ������ �����
  uint8_t BitMask = Row % 8;
  BitMask = (1 << BitMask);
  
  // ��������� 8 ���
  for (uint8_t Bit = 0; Bit < 8; Bit++)
  {
    if (pBuff[ByteIdx + Bit] & BitMask)
      Byte |= (1 << (7 - Bit));
  }
  
  return Byte;
}
//==============================================================================


//==============================================================================
// ��������� ��������� ����� ������������ ������ ������� � ������������ � ������� pBuff
//==============================================================================
void ST7920_DisplayFullUpdate(uint8_t *pBuff, uint16_t BuffLen)
{
  for (uint8_t Row = 0; Row < 32; Row++)
  {
    // ����� �� ������ ��������� ������ � ������ �������
    ST7920_Ext_SetGDRAMAddr(Row, 0);
    
    // ������� ������ ������� 1 ������� � ������� �������� �������
    for (uint8_t Col = 0; Col < 16; Col++)
      ST7920_write(0, ST7920_GetHorizontalByte(pBuff, Row, Col));
    // ������� ������ ������� 1 ������� � ������ �������� �������
    for (uint8_t Col = 0; Col < 16; Col++)
      ST7920_write(0, ST7920_GetHorizontalByte(pBuff, Row + 32, Col));
  }
}
//==============================================================================


//==============================================================================
// ��������� ��������� ����� ���������������� ��� ������ � ST7920
//==============================================================================
void ST7920_GPIO_init(void)
{
  ST7920_RESET_HIGH();
  ST7920_RS_CS_LOW();

  // ����������� ��� ������������ ���� ��� ������
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
  
#if (ST7920_RESET_Used)
  // ������ RESET
  gpio_PortClockStart(ST7920_RESET_Port);
  GPIO_InitStruct.GPIO_Pin = ST7920_RESET_Pin;
  GPIO_Init(ST7920_RESET_Port, &GPIO_InitStruct);
#endif
  
#if (ST7920_RS_CS_Used)
  // ������ RS (��� ������������� ����������) / CS (��� SPI)
  gpio_PortClockStart(ST7920_RS_CS_Port);
  GPIO_InitStruct.GPIO_Pin = ST7920_RS_CS_Pin;
  GPIO_Init(ST7920_RS_CS_Port, &GPIO_InitStruct);
#endif
  
#if (ST7920_IF != ST7920_IF_SPI)        // ������������ ���������?
  ST7920_RW_LOW();
  ST7920_E_LOW();

  // ������ RW 
  gpio_PortClockStart(ST7920_RW_Port);
  GPIO_InitStruct.GPIO_Pin = ST7920_RW_Pin;
  GPIO_Init(ST7920_RW_Port, &GPIO_InitStruct);
  // ������ E
  gpio_PortClockStart(ST7920_E_Port);
  GPIO_InitStruct.GPIO_Pin = ST7920_E_Pin;
  GPIO_Init(ST7920_E_Port, &GPIO_InitStruct);

  // ����� ������ ������������ ����
  gpio_PortClockStart(ST7920_Data_Port);
  #if (ST7920_IF == ST7920_IF_Parallel_4bit)
    GPIO_InitStruct.GPIO_Pin = (0x0F << ST7920_Data_Shift);
  #else
    GPIO_InitStruct.GPIO_Pin = 0xFF;
  #endif
  GPIO_Init(ST7920_Data_Port, &GPIO_InitStruct);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ���������� ������� ������������� ������� � ��������� ������
//==============================================================================
void ST7920_InitInTextMode(void)
{
  // ��������� ������������� ����������� � ������ Basic
  ST7920_Basic_FunctionSet(0);
  // �������� ������� ��� �������
  ST7920_Basic_DisplayOnOff(1, 0, 0);
  // ������� ��������� ����� �����
  ST7920_Basic_Clear();
  // �������� ��������� ������� ������
  ST7920_Basic_EntryMode(0, 1);
}
//==============================================================================


//==============================================================================
// ��������� ���������� ������� ������������� ������� � ����������� ������
//==============================================================================
void ST7920_InitInGraphMode(void)
{
  // ����������� ���������� ������� �� ������ Basic � Extended
  ST7920_Basic_FunctionSet(1);
  // �������� ����������� �����
  ST7920_Ext_FunctionSet(1, 1);
}
//==============================================================================


//==============================================================================
// ��������� ������������� �������
//==============================================================================
void ST7920_Init(uint8_t Width, uint8_t Height)
{
  ST7920_Width = Width;
  ST7920_Height = Height;

  // ������������� ����� ���������� ���������� ���������
  ST7920_GPIO_init();

#if (ST7920_IF == ST7920_IF_SPI)
  // ������������� ���������� SPI
  spim_init(ST7920_SPI_periph, 8);
#endif
  
  // �������� ����� ������ �������
  delay_ms(40);
  
  // ����� �������
#if (ST7920_RESET_Used)
  ST7920_RESET_LOW();
  delay_ms(10);
  ST7920_RESET_HIGH();
#endif
  
#if (ST7920_IF == ST7920_IF_Parallel_4bit)
  Init4bit = 1;
#endif
  // ������������� �������� ������������� ����������
  ST7920_Basic_FunctionSet(0);
  Init4bit = 0;  

  //ST7920_InitInTextMode();    // ������������� ������� � ��������� ������
  ST7920_InitInGraphMode();     // ������������� ������� � ����������� ������
}
//==============================================================================
