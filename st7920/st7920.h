//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _ST7920_H
#define _ST7920_H

#include <types.h>



// �������� ���������� ������ � ��������
#define ST7920_IF_Parallel_4bit 0
#define ST7920_IF_Parallel_8bit 1
#define ST7920_IF_SPI           2


// ������������ ��� ����������� � ������� ���������
#define ST7920_IF               ST7920_IF_SPI
#define ST7920_RESET_Used       0       // ������������ ���������� ������ RESET

// �������� � ������ ���������� ������� � ���
/// ������������ 4����
#define ST7920_ShortDelayUs_1   3
#define ST7920_ShortDelayUs_2   10
/// ������������ 8���
#define ST7920_ShortDelayUs_3   8
#define ST7920_ShortDelayUs_4   20
/// SPI
#define ST7920_ShortDelayUs_5   25



#if (ST7920_IF == ST7920_IF_SPI)
  #define ST7920_StartByte_RWmask       (1 << 2)
  #define ST7920_StartByte_RSmask       (1 << 1)

  // ������������ SPI (SPI1, SPI2)
  #define ST7920_SPI_periph       SPI1
  
  #define ST7920_RS_CS_Used     1       // ����� ���������� � 0 ���� ������� ���� � �� CS ���. 1
#else
  #define ST7920_RS_CS_Used     1
#endif

//==============================================================================
// ��������� ����������� � ���������� �������
//==============================================================================
// ������ RESET
#if (ST7920_RESET_Used)
  #define ST7920_RESET_Port     GPIOB
  #define ST7920_RESET_Pin      GPIO_Pin_11
#endif

// ������ RS (��� ������������� ����������) / CS (��� SPI)
#define ST7920_RS_CS_Port       GPIOB
#define ST7920_RS_CS_Pin        GPIO_Pin_9

#if (ST7920_IF != ST7920_IF_SPI)        // ������������ ���������?
  // ������ RW (1 - ������, 0 - ������)
  #define ST7920_RW_Port        GPIOB
  #define ST7920_RW_Pin         GPIO_Pin_8
  // ������ E (����� ��� ������������� ����������)
  #define ST7920_E_Port         GPIOB
  #define ST7920_E_Pin          GPIO_Pin_7
  // ���� ������ ������������� ���������� hd44780
  #define ST7920_Data_Port      GPIOA
  #if (ST7920_IF == ST7920_IF_Parallel_4bit)
    #define ST7920_Data_Shift          0      // ������� ����� ������������ ���� ����� �� ����� ��
  #endif
#endif
//==============================================================================
// ����� ������ Basic
#define ST7920_CmdBasic_Clear                   0x01    // ������� DDRAM
#define ST7920_CmdBasic_Home                    0x02    // ������� ������� � ������ �������
#define ST7920_CmdBasic_EntryMode               0x04    // ��������� ���������� ������� � ������
#define ST7920_CmdBasic_DisplayOnOff            0x08    // ���������� �������� � ��������
#define ST7920_CmdBasic_CursorDisplayControl    0x10    // 
#define ST7920_CmdBasic_FunctionSet             0x20    // ����� �������� ���������� � ���������� Extended Mode 
#define ST7920_CmdBasic_SetCGRAMaddr            0x40    // ��������� ������ � CGRAM 
#define ST7920_CmdBasic_SetDDRAMaddr            0x80    // ��������� ������ � DDRAM
// ����� ������ Extended
#define ST7920_CmdExt_StandBy                   0x01    // ������� � ����� StandBy
#define ST7920_CmdExt_SelScrollOrRamAddr        0x02    // ����� ������ ���� ������ � ������
#define ST7920_CmdExt_Reverse                   0x04    // ������ ����� �� 4 ����� � DDRAM
#define ST7920_CmdExt_FunctionSet               0x20    // ����� �������� ����������, ���������� Extended Mode � Graphic Mode
#define ST7920_CmdExt_SetIRAMOrSccrollAddr      0x40    // ��������� ������ � IRAM ��� ����� �������
#define ST7920_CmdExt_SetGDRAMAddr              0x80    // ��������� ������ � GDRAM (������ ������������ ������)
//==============================================================================


// ��������� ������������� �������
void ST7920_Init(uint8_t Width, uint8_t Height);
// ��������� ��������� ����� ������������ ������ ������� � ������������ � ������� pBuff
void ST7920_DisplayFullUpdate(uint8_t *pBuff, uint16_t BuffLen);

#endif
